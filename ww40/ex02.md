**De 3 måder at sniffe netværk på**

1: Sniff on interface opererer i layer 2. Man kan opsætte en proces på en client, der sniffer trafikken mellem netværks interfacet og browseren. Her vil man finde enkrypteret information.  
  
2: Mirror port opererer i layer 2. På en switch kan man opsætte en opstilling, der spejler al trafik og afvikler og monitorerer det i en "collector"  
  
3: Tap opererer i layer 1. Man kan installere et stykke hardware, der omdirigerer den ønskede trafik igennem sig og sender en kopi til en collector. Fungerer lidt ligesom metode nr. 2. Dette device kan både være wireless og cabled.  
  
  
    
**Vigtig side note1**: Det er vigtigt at tænke over hvilket interface man sniffer på, hvis man sniffer på en router, da en router for det meste har mange forbindelser kørende.

**Vigtig side note2**: Når man sniffer netværk kan man opsætte op til flere af disse metoder og forbinde dem til samme collector, for at sikre man får det hele med.