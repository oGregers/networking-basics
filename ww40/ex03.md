Command: IP address  
  
Kommandoen viser IP og MAC adresserne for min enheds netværks interfaces.  
  
Command: IP route  
  
Kommandoen viser IP adressen for min enhed og routeren den er forbundet til.  
  
  Screendumps er vedlagt under ww40/IP_address_cmd og ww40/IP_route_cmd
